import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useEffect} from 'react';

export default function Logout() {
	
	const { unsetUser, setUser} = useContext(UserContext);
	/*
	"localStorage.clear()" is a method that allows us to clear tyhe information in the localStorage ensuring that no information is stored in our browser
	*/
/*	localStorage.clear();*/
	unsetUser();

	useEffect(() =>{
		setUser({email: null});
	})

	return (
		<Navigate to = '/login'/>
	)
}