import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfullt binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e) {

		// Prevents the page reloading
		e.preventDefault();

		// Clear input fields
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!');
	}

	// Validation to enable submit button when all input fields are populated and both passwords match
	useEffect(() => {
		if((email !== '' && password1 !== '' & password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);

	return	(
		(user.email !== null)
		?
			<Navigate to="/courses"/>
		:

		// Invokes the registerUser function upon clicking on submit button
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>			
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>			
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>		
			</Form.Group>

			{/*conditional render submit button based on isActive state*/}

			{isActive	
				?
					<Button variant	="primary" type="submit" id="submitBtn">Submit</Button>
				:
					<Button variant	="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
			
		</Form>
	)
}
