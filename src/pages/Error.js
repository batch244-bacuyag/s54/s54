import Banner from '../components/Banner';

export default function Error() {

  const data = {
    title : "404 - Not Found",
    content : "The page you are looking for cannot be found.",
    destination: "/",
    label: "Back home"
  }

  return (
    <Banner data={data}/>
  )
}


/*export default function ErrorPage(){
  return(
    <Row>
      <Col className = "p-5 text-center">
        <h4>Zuitt Booking</h4>
        <h1>Page Not Found</h1>
        <p>Go back to the <Link to="/">homepage</Link>.</p>
      </Col>
    </Row>
  )
}*/