import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {

  // Deconstruct the course properties into their own variables
    const { name, description, price} = courseProp;

  //Syntax:
  //const [getter, setter] = useState(initialGetterValue)
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [ isOpen, setIsOpen] = useState(false);

  //Function that keeps track of the enrollees for a course
/*    function enroll(){
      if (count < seats) {
        setCount(count + 1);
        console.log('Enrollees ' + count);
      } else {
        alert("no more seats");
        setDisabled(true);
      }
    }

*/

    function enroll(){
/*      if (count < seats) {*/
        setCount(count + 1);
        console.log('Enrollees ' + count);
        setSeats(seats - 1);
        console.log('Seats' + seats);
/*      } else {
        alert("no more seats");
        setDisabled(true);
      }*/
    }
/*    const enroll = () => {
      count < seats 
        ? (setCount(count + 1), console.log('Enrollees ' + count))
        : (alert("no more seats"), setDisabled(true));
    };*/

// Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
            // This is run automatically both after initial render and for every DOM update
          // React will re-run this effect ONLY if any of the values contained in the seats array has changed from the last render / update
    useEffect(() =>{
      if(seats === 0){
        setIsOpen(true);
      }
    }, [seats]);

    return (
      <Card>
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <Card.Text>{count} Enrollees</Card.Text>
              <Button variant="primary" onClick={enroll} disabled={isOpen}>
                Enroll
              </Button>
          </Card.Body>
      </Card>  

    )
}

//alternate code 
/*import {useState} from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {

  // Deconstruct the course properties into their own variables
    const { name, description, price} = courseProp;

  //Syntax:
  //const [getter, setter] = useState(initialGetterValue)
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);
    const [disabled, setDisabled] = useState(false);

  //Function that keeps track of the enrollees for a course
    function enroll(){
      if (count < seats) {
        setCount(count + 1);
        console.log('Enrollees ' + count);
      } else {
        alert("no more seats");
        setDisabled(true);
      }
    }

    return (
      <Card>
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <Card.Text>Seats left: {seats - count}</Card.Text>
              <Button variant="primary" onClick={enroll} disabled={disabled}>
                Enroll
              </Button>
          </Card.Body>
      </Card>  

    )
}*/
