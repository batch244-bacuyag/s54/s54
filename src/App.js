// import { Fragment } from 'react';
import { useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';
import { UserProvider } from './UserContext';


function App() {
    
    // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({email: localStorage.getItem('email')});

    // Function for clearing localStorage on logout
    const unsetUser = () => {
        localStorage.clear();
    }

  return(
    // In React JS, multiple components rendered in a single component should be wrapped in a parent component
    // "Fragment" ensures that an error will be prevented

    // We store information in the context by providing the information using the "UserProvider" component and passing the information via the "value" prop
    // All information inside the value prop will be accessible to pages/components wrapped around the UserProvider.

    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
            <AppNavbar/>
            {/*<Home/>*/}
            <Container>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/courses" element={<Courses/>} />
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/register" element={<Register/>} />
                    <Route path="/logout" element={<Logout/>} />
                {/*{/*"*" is used to render paths that are not found in our routing system/}*/}
                    <Route path="*" element={<Error />} />
                </Routes>
            </Container>         
        </Router>
    </UserProvider>
  );
}

export default App;
