//We will use React's context API to give the 'global' scope within our application

import React from 'react';

//create context
//a context with object data type ythat can be used to store information that can be shared to other components wihtin the app

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;